const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const fetch = require('node-fetch'); // Install with: npm install node-fetch
const { execSync } = require('child_process');
const addon = require('./build/Release/fibonacciaddon');

const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

app.use(express.static('public'));

wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    const number = parseInt(message);

    if (isNaN(number) || number <= 0) {
      ws.send(JSON.stringify({ error: 'Invalid input. Please enter a positive number.' }));
      return;
    }

    const fibonacciSequence = addon.fibonacci(number);
    ws.send(JSON.stringify({ result: fibonacciSequence }));
  });
});

const PORT = process.env.PORT || 3001;
server.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

// Example Jest test
describe('WebSocket Server', () => {
  let server;

  beforeAll(() => {
    server = app.listen(3000, () => console.log(`Listening at port ${3000}`));
  });

  afterAll(() => {
    server.close()
  });

  test('should respond with correct Fibonacci sequence for a valid input', async () => {
    const ws = new WebSocket(`ws://localhost:${PORT}`);

    await new Promise((resolve) => ws.on('open', resolve));

    const input = 5;
    ws.send(input.toString());

    const response = await new Promise((resolve) => {
      ws.on('message', (data) => resolve(JSON.parse(data)));
    });

    expect(response.result).toBeDefined();
    expect(Array.isArray(response.result)).toBe(true);
    expect(response.result).toEqual([0, 1, 1, 2, 3]);
  });

  test('should respond with an error for an invalid input', async () => {
    const ws = new WebSocket(`ws://localhost:${PORT}`);

    await new Promise((resolve) => ws.on('open', resolve));

    const input = 'invalid';
    ws.send(input);

    const response = await new Promise((resolve) => {
      ws.on('message', (data) => resolve(JSON.parse(data)));
    });

    expect(response.error).toBeDefined();
    expect(response.result).toBeUndefined();
  });
});
