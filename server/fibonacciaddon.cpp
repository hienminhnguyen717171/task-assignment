#include <napi.h>

// C++ function to generate the Fibonacci sequence (dynamic programming)
/**
 * Time complexity: 0(n)
 * Space complexity: 0(n)
 * 
 * 
 * 
 * Optimize the algorithm for performance, considering large input numbers. Ensure the
 * sequence generation process does not block or slow down the server's responsiveness.
 * 
 * 1. Matrix multiplication/exponentiation method - Time to O(log(N)) by computing the multiplication of N matrices.
 * matrix where Fn denotes the nth term of Fibonacci sequence.
 * 
 * 
 * 
 * 2. Using a formula, we directly implement the formula for the nth term in the Fibonacci series. 
 * Time O(1) Space O(1) Fn = {[(√5 + 1)/2] ^ n} / √5


Ref: 
https://stackoverflow.com/questions/10924096/optimization-of-fibonacci-sequence-generating-algorithm
https://stackoverflow.com/questions/14661633/finding-out-nth-fibonacci-number-for-very-large-n


 * 
*/
std::vector<int> fibonacci(int n) {
    std::vector<int> result;
    if (n <= 0) {
        return result;
    }

    result.push_back(0);
    if (n == 1) {
        return result;
    }

    result.push_back(1);
    for (int i = 2; i < n; ++i) {
        result.push_back(result[i - 1] + result[i - 2]);
    }

    return result;
}

// Wrapper function to be called from Node.js
Napi::Array Fibonacci(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsNumber()) {
        Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
        return Napi::Array::New(env);
    }

    int n = info[0].As<Napi::Number>().Int32Value();
    std::vector<int> result = fibonacci(n);

    Napi::Array arr = Napi::Array::New(env, result.size());
    for (size_t i = 0; i < result.size(); ++i) {
        arr.Set(i, result[i]);
    }

    return arr;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(Napi::String::New(env, "fibonacci"), Napi::Function::New(env, Fibonacci));
    return exports;
}

NODE_API_MODULE(fibonacciaddon, Init)
