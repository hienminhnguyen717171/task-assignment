const express = require('express');
const http = require('http');
const WebSocket = require('ws');

const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

const addon = require('./build/Release/fibonacciaddon');

app.use(express.static('public'));

wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    const number = parseInt(message);

    if (isNaN(number) || number <= 0) {
      ws.send(JSON.stringify({ error: 'Invalid input. Please enter a positive number.' }));
      return;
    }

    const fibonacciSequence = addon.fibonacci(number);
    ws.send(JSON.stringify({ result: fibonacciSequence }));
  });
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
